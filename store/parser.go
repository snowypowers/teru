package store

import (
	"encoding/json"
	"log"
	"strings"
	"time"
)

var areaNames = []string{
	"Ang Mo Kio",
	"Bedok",
	"Bishan",
	"Boon Lay",
	"Bukit Batok",
	"Bukit Merah",
	"Bukit Panjang",
	"Bukit Timah",
	"Central Water Catchment",
	"Changi",
	"Choa Chu Kang",
	"Clementi",
	"City",
	"Geylang",
	"Hougang",
	"Jalan Bahar",
	"Jurong East",
	"Jurong Island",
	"Jurong West",
	"Kallang",
	"Lim Chu Kang",
	"Mandai",
	"Marine Parade",
	"Novena",
	"Pasir Ris",
	"Paya Lebar",
	"Pioneer",
	"Pulau Tekong",
	"Pulau Ubin",
	"Punggol",
	"Queenstown",
	"Seletar",
	"Sembawang",
	"Sengkang",
	"Sentosa",
	"Serangoon",
	"Southern Islands",
	"Sungei Kadut",
	"Tampines",
	"Tanglin",
	"Tengah",
	"Toa Payoh",
	"Tuas",
	"Western Islands",
	"Western Water Catchment",
	"Woodlands",
	"Yishun"}

var areaCodes = map[string]string {
	"Amk": "Ang Mo Kio",
	"Bd": "Bedok",
	"Bs": "Bishan",
	"Bl": "Boon Lay",
	"Bkb": "Bukit Batok",
	"Bkm": "Bukit Merah",
	"Bkp": "Bukit Panjang",
	"Bkt": "Bukit Timah",
	"Ctw": "Central Water Catchment",
	"Ci": "Changi",
	"Cck": "Choa Chu Kang",
	"Cmt": "Clementi",
	"Ct": "City",
	"Gl": "Geylang",
	"Hg": "Hougang",
	"Jlb": "Jalan Bahar",
	"Je": "Jurong East",
	"Ji": "Jurong Island",
	"Jw": "Jurong West",
	"Kl": "Kallang",
	"Lck": "Lim Chu Kang",
	"Md": "Mandai",
	"Mp": "Marine Parade",
	"Nvn": "Novena",
	"Psr": "Pasir Ris",
	"Pyl": "Paya Lebar",
	"Pn": "Pioneer",
	"Plt": "Pulau Tekong",
	"Plu": "Pulau Ubin",
	"Pg": "Punggol",
	"Qt": "Queenstown",
	"Slt": "Seletar",
	"Sbw": "Sembawang",
	"Sk": "Sengkang",
	"Sts": "Sentosa",
	"Srg": "Serangoon",
	"Sti": "Southern Islands",
	"Sgk": "Sungei Kadut",
	"Tpn": "Tampines",
	"Tl": "Tanglin",
	"Tg": "Tengah",
	"Tpy": "Toa Payoh",
	"T": "Tuas",
	"Wti": "Western Islands",
	"Wtw": "Western Water Catchment",
	"Wl": "Woodlands",
	"Ys": "Yishun"}

//WF2 Raw data struct
type WF2 struct {
	AreaMetadata []struct {
		Name          string `json:"name"`
		LabelLocation struct {
			Latitude  float64 `json:"latitude"`
			Longitude float64 `json:"longitude"`
		} `json:"label_location"`
	} `json:"area_metadata"`
	Items []struct {
		UpdateTimestamp time.Time `json:"update_timestamp"`
		Timestamp       time.Time `json:"timestamp"`
		ValidPeriod     struct {
			Start time.Time `json:"start"`
			End   time.Time `json:"end"`
		} `json:"valid_period"`
		Forecasts []struct {
			Area     string `json:"area"`
			Forecast string `json:"forecast"`
		} `json:"forecasts"`
	} `json:"items"`
	APIInfo struct {
		Status string `json:"status"`
	} `json:"api_info"`
}

//WF2Update parsed formatted data
type WF2Update struct {
	Timestamp time.Time
	Forecasts map[string]string
}

//ParseWf2 into a WF2Update
func ParseWf2(data []byte) WF2Update {
	var record WF2
	if err := json.Unmarshal(data, &record); err != nil {
		log.Println(err)
	}
	ts := record.Items[0].Timestamp
	forecasts := make(map[string]string)
	update := WF2Update{ts, forecasts}
	for _, i := range record.Items[0].Forecasts {
		forecasts[i.Area] = i.Forecast
	}
	return update
}

//ParseArea parses string into a possible Area.
//Returns empty string if no matches
func ParseArea(args string) string {
	sanitised := strings.Trim(args, " .")
	sanitised = strings.Title(sanitised)
	switch sanitised {
	case "North", "South", "East", "West", "Central", "All", "":
		return sanitised
	default:
		for _, i := range areaNames {
			if sanitised == i {
				return i
			}
		}
		for k,v := range areaCodes {
			if sanitised == k {
				return v
			}
		}
	}
	return ""
}
