package store

import (
	"testing"
)

func TestParseArea(t *testing.T) {
	a := ParseArea("Ang Mo Kio")
	if a != "Ang Mo Kio" {
		t.Fail()
	}
	b := ParseArea("bkp")
	if b != "Bukit Panjang" {
		t.Fail()
	}

}
