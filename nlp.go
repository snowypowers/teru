package main

import (
	"bytes"
	"encoding/json"
	"io/ioutil"
	"log"
	"net/http"
	"time"

	"gopkg.in/telegram-bot-api.v4"
)

type nlpResponse struct {
	ID        string    `json:"id"`
	Timestamp time.Time `json:"timestamp"`
	Lang      string    `json:"lang"`
	Result    struct {
		Source        string `json:"source"`
		ResolvedQuery string `json:"resolvedQuery"`
		Speech        string `json:"speech"`
		Action        string `json:"action"`
		Parameters    struct {
			Area       string `json:"area"`
			Command    string `json:"command"`
			Simplified string `json:"simplified"`
		} `json:"parameters"`
		Metadata struct {
			InputContexts             []interface{} `json:"inputContexts"`
			OutputContexts            []interface{} `json:"outputContexts"`
			IntentName                string        `json:"intentName"`
			IntentID                  string        `json:"intentId"`
			WebhookUsed               string        `json:"webhookUsed"`
			WebhookForSlotFillingUsed string        `json:"webhookForSlotFillingUsed"`
			Contexts                  []interface{} `json:"contexts"`
		} `json:"metadata"`
		Score int `json:"score"`
	} `json:"result"`
	Status struct {
		Code      int    `json:"code"`
		ErrorType string `json:"errorType"`
	} `json:"status"`
	SessionID string `json:"sessionId"`
}

type nlpRequest struct {
	Query     string `json:"query"`
	SessionID string `json:"sessionId"`
	Lang      string `json:"lang"`
	V         int    `json:"v"`
}

func processNLP(update tgbotapi.Update) tgbotapi.MessageConfig {
	req := constructNLPReq(update)
	res, err := client.Do(req)
	if err != nil {
		log.Panic(err)
	}
	defer res.Body.Close()
	bs, err := ioutil.ReadAll(res.Body)
	var resBody nlpResponse
	if err := json.Unmarshal(bs, &resBody); err != nil {
		log.Println(err)
	}
	return nlpRespToMsg(update, resBody)
}

func constructNLPReq(update tgbotapi.Update) *http.Request {
	nlpBody := nlpRequest{update.Message.Text, string(update.Message.Chat.ID), "en", 20170101}
	body, err := json.Marshal(nlpBody)
	req, err := http.NewRequest("POST", "https://api.api.ai/v1/query", bytes.NewBuffer(body))
	if err != nil {
		log.Panic(err)
	}
	req.Header.Set("Authorization", "Bearer "+nlpToken)
	req.Header.Set("Content-Type", "application/json")
	return req
}

func nlpRespToMsg(update tgbotapi.Update, res nlpResponse) tgbotapi.MessageConfig {
	//log.Printf("NLP RESP: %v+", res)
	msg := tgbotapi.NewMessage(update.Message.Chat.ID, res.Result.Speech)
	return msg
}
